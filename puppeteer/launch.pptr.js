/**
 * 2024
 * STERAH
 */

const { url } = require('./utils/helpers.pptr');
const { initializeBrowser } = require('./browserUtil.pptr');
const { searchWeb, logLinks } = require('./searchWeb.pptr');

async function run () {
  const { /* browser */ page } = await initializeBrowser(url);

  await searchWeb(page);
  await logLinks(page);
}

run();
